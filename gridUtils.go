package aoc

type Grid struct {
	Cells    map[Tuple]rune
	Width    int
	Height   int
	Wrapping bool
}

func NewGrid(width, height int, wrapping bool) *Grid {
	cells := make(map[Tuple]rune)
	return &Grid{Width: width, Height: height, Cells: cells, Wrapping: wrapping}
}

func NewGridFromFile(filename string, wrapping bool) *Grid {
	lines := ReadInputLines(filename)
	height := len(lines)
	width := len(lines[0])
	grid := NewGrid(width, height, wrapping)
	for y, line := range lines {
		for x, c := range line {
			grid.Set(x, y, c)
		}
	}
	return grid
}

func (g *Grid) Get(x, y int) (rune, bool) {
	if x < 0 || x >= g.Width {
		if g.Wrapping {
			x = (g.Width + x) % g.Width
		} else {
			return 0, false
		}
	}
	if y < 0 || y >= g.Height {
		if g.Wrapping {
			y = (g.Height + y) % g.Height
		} else {
			return 0, false
		}
	}
	readValue, ok := g.Cells[Tuple{x, y}]
	return readValue, ok
}

func (g *Grid) GetAt(t Tuple) (rune, bool) {
	return g.Get(t.X, t.Y)
}

func (g *Grid) Set(x, y int, value rune) bool {
	if x < 0 {
		if g.Wrapping {
			x = (g.Width + x) % g.Width
		} else {
			return false
		}
	}
	if y < 0 {
		if g.Wrapping {
			y = (g.Height + y) % g.Height
		} else {
			return false
		}
	}
	g.Cells[Tuple{x, y}] = value
	return true
}

func (g *Grid) SetAt(t Tuple, value rune) bool {
	return g.Set(t.X, t.Y, value)
}

func (g *Grid) Count(criteria func(int, int, rune) bool) int {
	result := 0
	for y := 0; y < g.Height; y++ {
		for x := 0; x < g.Width; x++ {
			valXY, ok := g.Get(x, y)
			if ok && criteria(x, y, valXY) {
				result++
			}
		}
	}
	return result
}

func (g *Grid) CountNeighbors(x, y int, criteria func(int, int, rune) bool) int {
	result := 0
	for dy := -1; dy <= 1; dy++ {
		for dx := -1; dx <= 1; dx++ {
			if dx == 0 && dy == 0 {
				continue
			}
			valXY, ok := g.Get(x+dx, y+dy)
			if ok && criteria(x+dx, y+dy, valXY) {
				result++
			}
		}
	}
	return result
}

func (g *Grid) GetNeighbours(x, y int, criteria func(int, int, rune) bool) map[Tuple]rune {
	result := make(map[Tuple]rune)
	for dy := -1; dy <= 1; dy++ {
		for dx := -1; dx <= 1; dx++ {
			if dx == 0 && dy == 0 {
				continue
			}
			valXY, ok := g.Get(x+dx, y+dy)
			if ok && criteria(x+dx, y+dy, valXY) {
				result[Tuple{x + dx, y + dy}] = valXY
			}
		}
	}
	return result
}

func (g *Grid) CountNeighboursAroundZone(xMin, xMax, yMin, yMax int, criteria func(int, int, rune) bool) int {
	result := 0
	for y := yMin - 1; y <= yMax+1; y++ {
		for x := xMin - 1; x <= xMax+1; x++ {
			if x == xMin-1 || x == xMax+1 || y == yMin-1 || y == yMax+1 {
				valXY, ok := g.Get(x, y)
				if ok && criteria(x, y, valXY) {
					result++
				}
			}
		}
	}
	return result
}

func (g *Grid) GetNeighboursAroundZone(xMin, xMax, yMin, yMax int, criteria func(int, int, rune) bool) map[Tuple]rune {
	result := make(map[Tuple]rune)
	for y := yMin - 1; y <= yMax+1; y++ {
		for x := xMin - 1; x <= xMax+1; x++ {
			if x == xMin-1 || x == xMax+1 || y == yMin-1 || y == yMax+1 {
				valXY, ok := g.Get(x, y)
				if ok && criteria(x, y, valXY) {
					result[Tuple{x, y}] = valXY
				}
			}
		}
	}
	return result
}

func (g *Grid) FindAll(criteria func(int, int, rune) bool) []Tuple {
	result := make([]Tuple, 0)
	for y := 0; y < g.Height; y++ {
		for x := 0; x < g.Width; x++ {
			valXY, ok := g.Get(x, y)
			if ok && criteria(x, y, valXY) {
				result = append(result, Tuple{x, y})
			}
		}
	}
	return result
}
