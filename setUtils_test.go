package aoc

import (
	"sort"
	"testing"
)

func TestSet(t *testing.T) {
	s := NewSet[string]()

	s.Add("Peter")
	s.Add("David")
	s.Add("Max")

	if !s.Contains("Peter") {
		t.Error()
	}
	if s.Contains("George") {
		t.Error()
	}

	s.Remove("David")
	if s.Contains("David") {
		t.Error()
	}

	if s.Size() != 2 {
		t.Error()
	}

	s.AddAll([]string{"Joey", "Chandler", "Ross"})
	if s.Size() != 5 {
		t.Error()
	}

	s.AddN("Monica", "Rachel", "Phoebe")
	if s.Size() != 8 {
		t.Error()
	}

	s.Remove("Someone who isn't there")
	if s.Size() != 8 {
		t.Error()
	}

	s2 := NewSet[string]()
	s2.AddN("Joey", "Rachel", "Ross", "Jean-Louis", "Brett")

	if s.Intersection(s2).Size() != 3 {
		t.Error()
	}
	if s.Union(s2).Size() != 10 {
		t.Error()
	}
	if s.Difference(s2).Size() != 5 {
		t.Error()
	}

	c := 0
	for range s.Iterate() {
		c++
	}
	if c != 8 {
		t.Error()
	}

	nbs := NewSet[int]()
	nbs.AddAll([]int{5, 4, 3, 2, 1, 5, 4, 3, 2, 1})
	c = 0
	for range nbs.Iterate() {
		c++
	}
	if c != 5 {
		t.Error()
	}

	nbsSorted := nbs.ToList()
	sort.Ints(nbsSorted)
	if !ListEqual(nbsSorted, []int{1, 2, 3, 4, 5}) {
		t.Error()
	}
}
