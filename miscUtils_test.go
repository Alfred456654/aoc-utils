package aoc

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPgcd(t *testing.T) {
	assert.Equal(t, 12, Pgcd(24, 36))
	assert.Equal(t, 1, Pgcd(25, 36))
	assert.Equal(t, 2, Pgcd(26, 36))
}

func TestPpcm(t *testing.T) {
	assert.Equal(t, 35, Ppcm(5, 7))
	assert.Equal(t, 72, Ppcm(24, 36))
}
