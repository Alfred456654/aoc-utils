package aoc

type Tuple struct {
	X int
	Y int
}

type TupleSet struct {
	m    map[Tuple]struct{}
	Xmin int
	Xmax int
	Ymin int
	Ymax int
}

func (s *TupleSet) Clone() *TupleSet {
	return &TupleSet{
		m:    copyMap(s.m),
		Xmin: s.Xmin,
		Xmax: s.Xmax,
		Ymin: s.Ymin,
		Ymax: s.Ymax,
	}
}

func copyMap(m map[Tuple]struct{}) map[Tuple]struct{} {
	c := make(map[Tuple]struct{})
	for k, v := range m {
		c[k] = v
	}
	return c
}

func (t Tuple) Add(t2 Tuple) Tuple {
	result := Tuple{X: t.X + t2.X, Y: t.Y + t2.Y}
	return result
}

func (t Tuple) Sub(t2 Tuple) Tuple {
	return Tuple{X: t.X - t2.X, Y: t.Y - t2.Y}
}

func (t Tuple) Equals(t2 Tuple) bool {
	return t.X == t2.X && t.Y == t2.Y
}

func ContainsTuple(lst []Tuple, val Tuple) bool {
	for _, t := range lst {
		if t.Equals(val) {
			return true
		}
	}
	return false
}

func NewTupleSet() *TupleSet {
	return &TupleSet{m: make(map[Tuple]struct{})}
}

func (s *TupleSet) UpdateBoundaries() {
	var firstEl Tuple
	for firstEl = range s.m {
		break
	}
	s.Xmin = firstEl.X
	s.Xmax = firstEl.X
	s.Ymin = firstEl.Y
	s.Ymax = firstEl.Y
	for v := range s.m {
		s.Xmin = Min(v.X, s.Xmin)
		s.Xmax = Max(v.X, s.Xmax)
		s.Ymin = Min(v.Y, s.Ymin)
		s.Ymax = Max(v.Y, s.Ymax)
	}
}

func (s *TupleSet) Add(value Tuple) {
	s.m[value] = exists
}

func (s *TupleSet) AddN(values ...Tuple) {
	for _, v := range values {
		s.m[v] = exists
	}
}

func (s *TupleSet) AddAll(values []Tuple) {
	for _, v := range values {
		s.m[v] = exists
	}
}

func (s *TupleSet) Remove(value Tuple) {
	delete(s.m, value)
}

func (s *TupleSet) Contains(value Tuple) bool {
	_, c := s.m[value]
	return c
}

func (s *TupleSet) Size() int {
	return len(s.m)
}

func (s *TupleSet) Intersection(s2 *TupleSet) *TupleSet {
	result := NewTupleSet()
	for v := range s.m {
		if s2.Contains(v) {
			result.Add(v)
		}
	}
	return result
}

func (s *TupleSet) Union(s2 *TupleSet) *TupleSet {
	result := NewTupleSet()
	for v := range s.m {
		result.Add(v)
	}
	for v := range s2.m {
		result.Add(v)
	}
	return result
}

func (s *TupleSet) Difference(s2 *TupleSet) *TupleSet {
	result := NewTupleSet()
	for v := range s.m {
		if !s2.Contains(v) {
			result.Add(v)
		}
	}
	return result
}

func (s *TupleSet) Iterate() map[Tuple]struct{} {
	return s.m
}

func (s *TupleSet) ToList() []Tuple {
	result := make([]Tuple, len(s.m))
	c := 0
	for t := range s.m {
		result[c] = t
		c++
	}
	return result
}

type FastTuple struct {
	val int
	dim int
}

func NewFastTuple(x, y, dim int) FastTuple {
	return FastTuple{val: dim*x + y, dim: dim}
}

func (t FastTuple) X() int {
	return t.val / t.dim
}

func (t FastTuple) Y() int {
	return t.val % t.dim
}

func (t FastTuple) Add(t2 FastTuple) FastTuple {
	return FastTuple{
		val: t.val + t2.val,
		dim: t.dim,
	}
}

func (t FastTuple) Equals(t2 FastTuple) bool {
	return t.dim == t2.dim && t2.val == t.val
}

func TupleSortFunc(a Tuple, b Tuple) int {
	if a.X < b.X {
		return -1
	} else if a.X > b.X {
		return 1
	} else {
		if a.Y < b.Y {
			return -1
		} else if a.Y > b.Y {
			return 1
		} else {
			return 0
		}
	}
}
