package aoc

import (
	"fmt"
	"os"
)

func CheckEqualsMsg[T Applicable](expected, got T, msg string) {
	if expected != got {
		Log(fmt.Sprintf("%v: expected %%v, got %%v", msg), expected, got)
	}
}

func CheckEquals[T Applicable](expected, got T) {
	if expected != got {
		Log("expected %v, got %v", expected, got)
	}
}

func EnsureDirectory() {
	_, err := os.Stat("go.mod")
	if err != nil {
		err = os.Chdir("..")
		if err != nil {
			panic(err)
		}
		_, err = os.Stat("go.mod")
		if err != nil {
			err = os.Chdir("..")
			if err != nil {
				panic(err)
			}
			_, err = os.Stat("go.mod")
			if err != nil {
				panic(err)
			}
		}
	}

}
