package aoc

import (
	"golang.org/x/exp/constraints"
)

type Applicable interface {
	int | int64 | float64 | string
}

func Contains[T Applicable](values []T, value T) bool {
	for _, v := range values {
		if v == value {
			return true
		}
	}
	return false
}

func IndexOf[T Applicable](values []T, value T) int {
	for i, v := range values {
		if v == value {
			return i
		}
	}
	Log("could not find %v in provided array", value)
	return -1
}

func Count[T Applicable](values []T, value T) int {
	result := 0
	for _, v := range values {
		if v == value {
			result++
		}
	}
	return result
}

func ListMin[T constraints.Ordered](values []T) T {
	result := values[0]
	for _, v := range values {
		if v < result {
			result = v
		}
	}
	return result
}

func ListMax[T constraints.Ordered](values []T) T {
	result := values[0]
	for _, v := range values {
		if v > result {
			result = v
		}
	}
	return result
}

func ListEqual[T Applicable](a, b []T) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func DelIdx[T Applicable](idx int, values []T) []T {
	if idx >= len(values) {
		Log("list has length of %d, cannot remove item at index %d", len(values), idx)
		return values
	}
	return append(values[:idx], values[idx+1:]...)
}

func DelFirstVal[T Applicable](val T, values []T) []T {
	for i, v := range values {
		if v == val {
			return DelIdx(i, values)
		}
	}
	return values
}

func Sum[T constraints.Integer](values []T) T {
	var res T = 0
	for _, v := range values {
		res += v
	}
	return res
}

func Prod[T constraints.Integer](values []T) T {
	if len(values) == 0 {
		return 0
	}
	var res T = 1
	for _, v := range values {
		res *= v
	}
	return res
}
