package aoc

type StringStack struct {
	ar []string
}

func NewStringStack() *StringStack {
	return &StringStack{ar: make([]string, 0)}
}

func (q *StringStack) Pop() string {
	res := q.ar[0]
	q.ar = q.ar[1:]
	return res
}

func (q *StringStack) PopN(idx int) string {
	res := q.ar[idx]
	q.ar = append(q.ar[:idx], q.ar[idx+1:]...)
	return res
}

func (q *StringStack) Len() int {
	return len(q.ar)
}

func (q *StringStack) Push(s string) {
	q.ar = append(q.ar, s)
}

func (q *StringStack) PushN(s string, idx int) {
	begin := append([]string{}, q.ar[:idx]...)
	begin = append(begin, s)
	q.ar = append(begin, q.ar[idx:]...)
}

func (q *StringStack) PushAll(values []string) {
	q.ar = append(q.ar, values...)
}

func (q *StringStack) Array() []string {
	return q.ar
}
