package aoc

import (
	"os"
	"strconv"
	"strings"
)

func ReadInputIntegers(filename string) []int {
	lines := ReadInputLines(filename)
	result := make([]int, len(lines))
	var err2 error
	for i, l := range lines {
		result[i], err2 = strconv.Atoi(l)
		if err2 != nil {
			Err("could not parse '%s' as an integer", err2, l)
		}
	}
	return result
}

func ReadInputLines(filename string) []string {
	bytes, err := os.ReadFile(filename)
	if err != nil {
		Err("could not read file '%s'", err, filename)
	}
	allLines := string(bytes)
	linesRaw := strings.Split(allLines, "\n")
	lines := make([]string, len(linesRaw))
	rem := 0
	for i, line := range linesRaw {
		l := strings.Trim(line, " ")
		if len(l) > 0 {
			lines[i] = l
		} else {
			rem++
		}
	}
	return lines[:len(lines)-rem]
}

func ReadIntInputLines(filename string) [][]int {
	bytes, err := os.ReadFile(filename)
	if err != nil {
		Err("could not read file '%s'", err, filename)
	}
	allLines := string(bytes)
	linesRaw := strings.Split(allLines, "\n")
	grid := make([][]int, len(linesRaw))
	rem := 0
	for i, line := range linesRaw {
		l := strings.Trim(line, " ")
		if len(l) > 0 {
			numbers := strings.Split(l, " ")
			grid[i] = make([]int, len(numbers))
			for j, n := range numbers {
				grid[i][j], _ = strconv.Atoi(n)
			}
		} else {
			rem++
		}
	}
	return grid[:len(grid)-rem]
}

func ReadByBlocks(filename string) [][]string {
	bytes, _ := os.ReadFile(filename)
	blocks := strings.Split(string(bytes), "\n\n")
	result := make([][]string, len(blocks))
	for i, a := range blocks {
		linesRaw := strings.Split(a, "\n")
		result[i] = make([]string, 0)
		for _, line := range linesRaw {
			l := strings.Trim(line, " ")
			if len(l) > 0 {
				result[i] = append(result[i], l)
			}
		}
	}
	return result
}

func ReadSimpleList(filename string) []int {
	bytes, err := os.ReadFile(filename)
	if err != nil {
		Err("could not read file '%s'", err, filename)
	}
	wholeLine := string(bytes)
	chunks := strings.Split(wholeLine, ",")
	result := make([]int, len(chunks))
	for i, c := range chunks {
		result[i], _ = strconv.Atoi(c)
	}
	return result
}

func ReadIntRectangle(filename string) [][]int {
	bytes, err := os.ReadFile(filename)
	if err != nil {
		Err("could not read file '%s'", err, filename)
	}
	allLinesStr := string(bytes)
	allLines := strings.Split(allLinesStr, "\n")
	result := make([][]int, len(allLines))
	for i, line := range allLines {
		line = strings.Replace(line, " ", "", -1)
		if len(line) > 0 {
			result[i] = make([]int, len(line))
			for j, c := range line {
				result[i][j], _ = strconv.Atoi(string(c))
			}
		} else {
			result = result[:i]
		}
	}
	return result
}

func ReadIntsByBlocks(filename string) [][]int {
	bytes, err := os.ReadFile(filename)
	if err != nil {
		Err("could not read file '%s'", err, filename)
	}
	blocks := strings.Split(string(bytes), "\n\n")
	result := make([][]int, len(blocks))
	for i, b := range blocks {
		lines := strings.Split(b, "\n")
		result[i] = make([]int, len(lines))
		for j, l := range lines {
			val64, err2 := strconv.ParseInt(strings.TrimSpace(l), 10, 64)
			if err2 != nil {
				Err("cannot parse int '%s'", err2, l)
			}
			result[i][j] = int(val64)
		}
	}
	return result
}
