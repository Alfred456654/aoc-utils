package aoc

import "math"

func Min[T int | int64](a, b T) T {
	if a <= b {
		return a
	}
	return b
}

func Max[T int | int64](a, b T) T {
	if a >= b {
		return a
	}
	return b
}

func Abs[T int | int64](a T) T {
	if a < 0 {
		return -a
	}
	return a
}

func Pow[T int | int64](a, x T) T {
	return T(math.Pow(float64(a), float64(x)))
}

func Between[T int | int64](a, b, c T) bool {
	return a < b && b < c
}

func Mod[T int | int64](p, q T) T {
	return T(math.Mod(float64(p), float64(q)))
}

func Pgcd[T int | int64](a, b T) T {
	for b > 0 {
		a, b = b, a%b
	}
	return a
}

func Ppcm[T int | int64](a, b T) T {
	return a * b / Pgcd(a, b)
}
