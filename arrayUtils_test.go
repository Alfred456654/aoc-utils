package aoc

import "testing"

func ArrayUtils(t *testing.T) {
	a := []int{1, 2, 3, 1, 4, 6, 1, 7, 8}
	if Count(a, 1) != 3 {
		t.Error()
	}
}

func TestDelIntIdx(t *testing.T) {
	a := []int{1, 2, 3, 1, 4, 6, 1, 7, 8}
	removeIdx := 3
	expected := []int{1, 2, 3, 4, 6, 1, 7, 8}
	result := DelIdx(removeIdx, a)
	if !ListEqual(expected, result) {
		t.Error()
	}
}

func TestIntDelFirstVal(t *testing.T) {
	a := []int{1, 2, 3, 1, 4, 6, 1, 3, 8}
	removeVal := 3
	expected := []int{1, 2, 1, 4, 6, 1, 3, 8}
	result := DelFirstVal(removeVal, a)
	if !ListEqual(expected, result) {
		t.Error()
	}
}

func TestListMin(t *testing.T) {
	a := []int{5, -1, 2, 5, 3, 7}
	expected := -1
	result := ListMin(a)
	if result != expected {
		t.Error()
	}
}

func TestListMax(t *testing.T) {
	a := []int{5, -1, 2, 5, 3, 7}
	expected := 7
	result := ListMax(a)
	if result != expected {
		t.Error()
	}
}

func TestSum(t *testing.T) {
	a := []int{1, 2, 3, 4, 5}
	expected := 15
	result := Sum(a)
	if result != expected {
		t.Fail()
	}
}

func TestProd(t *testing.T) {
	a := []int{1, 2, 3, 4, 5}
	expected := 120
	result := Prod(a)
	if result != expected {
		t.Fail()
	}
	a = []int{}
	expected = 0
	result = Prod(a)
	if result != expected {
		t.Fail()
	}
}
