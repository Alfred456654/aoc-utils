package aoc

import (
	"testing"
)

func TestTuple_Add(t *testing.T) {
	a := Tuple{3, 5}
	b := Tuple{4, 6}
	c := a.Add(b)
	expected := Tuple{7, 11}
	if !c.Equals(expected) {
		t.Fail()
	}
}

func TestNewTupleSet(t *testing.T) {
	a := NewTupleSet()
	a.AddN(Tuple{1, 1}, Tuple{1, 2}, Tuple{1, 3}, Tuple{1, 4})
	if a.Size() != 4 {
		t.Fail()
	}
	a.Add(Tuple{1, 5})
	if a.Size() != 5 {
		t.Fail()
	}
	a.Add(Tuple{1, 3})
	if a.Size() != 5 {
		t.Fail()
	}
	b := NewTupleSet()
	b.AddAll([]Tuple{{1, 1}, {1, 2}, {2, 1}, {2, 2}})
	if b.Size() != 4 {
		t.Fail()
	}
	if !a.Contains(Tuple{1, 5}) {
		t.Fail()
	}
	a.UpdateBoundaries()
	if a.Ymax != 5 {
		t.Fail()
	}
	a.Remove(Tuple{1, 5})
	if a.Contains(Tuple{1, 5}) {
		t.Fail()
	}
	a.UpdateBoundaries()
	if a.Ymax != 4 {
		t.Fail()
	}
	inter := a.Intersection(b)
	if inter.Size() != 2 {
		t.Fail()
	}
	union := a.Union(b)
	if union.Size() != 6 {
		t.Fail()
	}
	diff := a.Difference(b)
	if diff.Size() != 2 {
		t.Fail()
	}
	count := 0
	for range a.Iterate() {
		count++
	}
	if count != 4 {
		t.Fail()
	}
	list := a.ToList()
	if len(list) != 4 {
		t.Fail()
	}
}

func TestNewFastTuple(t *testing.T) {
	a := NewFastTuple(-1, 5, 100)
	b := NewFastTuple(20, 3, 100)
	add := a.Add(b)
	if !add.Equals(NewFastTuple(19, 8, 100)) {
		t.Fail()
	}
}

func TestCloneTupleSet(t *testing.T) {
	a := NewTupleSet()
	a.Add(Tuple{X: 1, Y: 2})
	a.Add(Tuple{X: 3, Y: 4})
	b := a.Clone()
	b.Remove(Tuple{X: 1, Y: 2})
	b.Add(Tuple{X: 5, Y: 6})
	if a.Contains(Tuple{X: 5, Y: 6}) {
		t.Fail()
	}
	if !a.Contains(Tuple{X: 1, Y: 2}) {
		t.Fail()
	}
}
