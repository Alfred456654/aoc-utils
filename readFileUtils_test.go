package aoc

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestReadByBlocks(t *testing.T) {
	blocks := ReadByBlocks("testdata/blocks.txt")
	if len(blocks) != 3 {
		t.Fail()
	}
}

func TestReadInputIntegers(t *testing.T) {
	integers := ReadInputIntegers("testdata/integers.txt")
	if len(integers) != 10 {
		t.Fail()
	}
}

func TestReadInputLines(t *testing.T) {
	lines := ReadInputLines("testdata/lines.txt")
	if len(lines) != 5 {
		t.Fail()
	}
}

func TestReadSimpleList(t *testing.T) {
	list := ReadSimpleList("testdata/simplelist.txt")
	if len(list) != 5 {
		t.Fail()
	}
}

func TestReadIntRectangle(t *testing.T) {
	listEol := ReadIntRectangle("testdata/witheol.txt")
	listNoEol := ReadIntRectangle("testdata/noeol.txt")
	if len(listEol) != len(listNoEol) {
		t.Fail()
	}
}

func TestReadIntsByBlocks(t *testing.T) {
	intsByBlocks := ReadIntsByBlocks("testdata/intsByBlocks.txt")
	assert.Len(t, intsByBlocks, 5)
	assert.Len(t, intsByBlocks[3], 3)
}

func TestReadIntInputLines(t *testing.T) {
	ints := ReadIntInputLines("testdata/intlines.txt")
	assert.Len(t, ints, 8)
	assert.Len(t, ints[3], 7)
	assert.Equal(t, ints[5][3], 89)
}
