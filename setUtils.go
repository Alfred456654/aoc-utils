package aoc

var exists = struct{}{}

type Set[T Applicable] struct {
	m map[T]struct{}
}

func NewSet[T Applicable]() *Set[T] {
	s := &Set[T]{}
	s.m = make(map[T]struct{})
	return s
}

func (s *Set[T]) Add(value T) {
	s.m[value] = exists
}

func (s *Set[T]) AddN(values ...T) {
	for _, v := range values {
		s.m[v] = exists
	}
}

func (s *Set[T]) AddAll(values []T) {
	for _, v := range values {
		s.m[v] = exists
	}
}

func (s *Set[T]) Remove(value T) {
	delete(s.m, value)
}

func (s *Set[T]) Contains(value T) bool {
	_, c := s.m[value]
	return c
}

func (s *Set[T]) Size() int {
	return len(s.m)
}

func (s *Set[T]) Intersection(s2 *Set[T]) *Set[T] {
	result := NewSet[T]()
	for v := range s.m {
		if s2.Contains(v) {
			result.Add(v)
		}
	}
	return result
}

func (s *Set[T]) Union(s2 *Set[T]) *Set[T] {
	result := NewSet[T]()
	for v := range s.m {
		result.Add(v)
	}
	for v := range s2.m {
		result.Add(v)
	}
	return result
}

func (s *Set[T]) Difference(s2 *Set[T]) *Set[T] {
	result := NewSet[T]()
	for v := range s.m {
		if !s2.Contains(v) {
			result.Add(v)
		}
	}
	return result
}

func (s *Set[T]) Iterate() map[T]struct{} {
	return s.m
}

func (s *Set[T]) ToList() []T {
	result := make([]T, len(s.m))
	c := 0
	for i := range s.m {
		result[c] = i
		c++
	}
	return result
}
