package aoc

import (
	"github.com/stretchr/testify/assert"
	"golang.org/x/exp/slices"
	"testing"
)

func TestNewGrid(t *testing.T) {
	g := NewGrid(5, 7, false)
	assert.Equal(t, 5, g.Width)
	assert.Equal(t, 7, g.Height)
	assert.False(t, g.Wrapping)
}

func TestNewGridFromFile(t *testing.T) {
	EnsureDirectory()
	g := NewGridFromFile("testdata/grid.txt", false)
	assert.Equal(t, 10, g.Width)
	assert.Equal(t, 10, g.Height)
	assert.False(t, g.Wrapping)
	assert.Equal(t, '4', g.Cells[Tuple{0, 0}])
	assert.Equal(t, '+', g.Cells[Tuple{5, 5}])
}

func TestGrid_Get(t *testing.T) {
	g := NewGridFromFile("testdata/grid.txt", false)
	checkGet(g, 0, 0, '4', true, t)
	checkGet(g, 5, 5, '+', true, t)
	checkGet(g, 5, 15, 0, false, t)
}

func checkGet(g *Grid, x, y int, expected rune, expectPresent bool, t *testing.T) {
	val, ok := g.Get(x, y)
	if ok != expectPresent || val != expected {
		t.Errorf("expected %c (%d), got %c (%d)", expected, expected, val, val)
		t.Fail()
	}
}

func TestGrid_Set(t *testing.T) {
	g := NewGrid(5, 5, false)
	checkGet(g, 0, 4, 0, false, t)
	checkGet(g, 0, 4, 0, false, t)
	g.Set(0, 0, '4')
	g.Set(4, 4, '+')
	checkGet(g, 0, 0, '4', true, t)
	checkGet(g, 4, 4, '+', true, t)
	checkGet(g, 5, 5, 0, false, t)
}

func TestCount(t *testing.T) {
	g := NewGridFromFile("testdata/grid.txt", false)
	count := g.Count(func(x, y int, r rune) bool { return r == '.' })
	assert.Equal(t, 66, count)
}

func TestGrid_CountNeighbors(t *testing.T) {
	g := NewGridFromFile("testdata/grid.txt", false)
	count := g.CountNeighbors(3, 2, func(x, y int, r rune) bool { return r == '.' })
	assert.Equal(t, 6, count)
}

func TestGrid_GetNeighbours(t *testing.T) {
	g := NewGridFromFile("testdata/grid.txt", false)
	neighbours := g.GetNeighbours(3, 2, func(x, y int, r rune) bool { return true })
	expectedNeighbours := []rune{'.', '.', '.', '.', '.', '.', '3', '*'}
	neighbourRunes := make([]rune, len(neighbours))
	i := 0
	for _, r := range neighbours {
		neighbourRunes[i] = r
		i++
	}
	slices.Sort(neighbourRunes)
	slices.Sort(expectedNeighbours)
	assert.Equal(t, expectedNeighbours, neighbourRunes)
}

func TestGrid_CountNeighboursAroundZone(t *testing.T) {
	g := NewGridFromFile("testdata/grid.txt", false)
	count := g.CountNeighboursAroundZone(0, 2, 4, 4, func(x, y int, r rune) bool { return r == '.' })
	assert.Equal(t, 8, count)
}

func TestGrid_GetNeighboursAroundZone(t *testing.T) {
	g := NewGridFromFile("testdata/grid.txt", false)
	neighbours := g.GetNeighboursAroundZone(5, 7, 9, 9, func(x, y int, r rune) bool { return true })
	expectedNeighbours := []rune{'.', '.', '.', '.', '.', '.', '*'}
	neighbourRunes := make([]rune, len(neighbours))
	i := 0
	for _, r := range neighbours {
		neighbourRunes[i] = r
		i++
	}
	slices.Sort(neighbourRunes)
	slices.Sort(expectedNeighbours)
	assert.Equal(t, expectedNeighbours, neighbourRunes)
}

func TestWrapping(t *testing.T) {
	g := NewGridFromFile("testdata/grid.txt", true)
	checkGet(g, 0, 0, '4', true, t)
	checkGet(g, 5, 5, '+', true, t)
	checkGet(g, 5, 15, '+', true, t)
	checkGet(g, 15, 5, '+', true, t)
	checkGet(g, 15, 15, '+', true, t)
	g.Set(-5, -5, '6')
	checkGet(g, 5, 5, '6', true, t)
	checkGet(g, -5, -5, '6', true, t)
	neighbours := g.GetNeighbours(0, 0, func(x, y int, r rune) bool { return true })
	expectedNeighbours := []rune{'6', '6', '.', '.', '.', '.', '.', '.'}
	neighbourRunes := make([]rune, len(neighbours))
	i := 0
	for _, r := range neighbours {
		neighbourRunes[i] = r
		i++
	}
	slices.Sort(neighbourRunes)
	slices.Sort(expectedNeighbours)
	assert.Equal(t, expectedNeighbours, neighbourRunes)
}

func TestGrid_FindAll(t *testing.T) {
	g := NewGridFromFile("testdata/grid.txt", false)
	found := g.FindAll(func(x, y int, r rune) bool { return r == '*' })
	assert.Equal(t, 3, len(found))
	slices.SortFunc(found, TupleSortFunc)
	assert.Equal(t, Tuple{3, 1}, found[0])
	assert.Equal(t, Tuple{3, 4}, found[1])
	assert.Equal(t, Tuple{5, 8}, found[2])
}
