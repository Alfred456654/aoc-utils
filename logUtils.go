package aoc

import (
	"fmt"
	"log"
	"os"
)

var logger = log.New(os.Stderr, "", 0)

func Log(message string, args ...interface{}) {
	logger.Printf(fmt.Sprintf("%s\n", message), args...)
}

func Err(message string, err error, args ...interface{}) {
	logger.Printf(fmt.Sprintf("%s\n", message), args...)
	logger.Println(err)
}
