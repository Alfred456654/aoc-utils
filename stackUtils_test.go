package aoc

import (
	"testing"
)

func TestNewStringQueue(t *testing.T) {
	q := NewStringStack()
	array := []string{"a", "b", "c", "d", "e"}
	q.PushAll(array)
	if q.Len() != len(array) {
		t.Fail()
	}
	q.Push("f")
	if q.Len() != len(array)+1 {
		t.Fail()
	}
	a := q.Pop()
	if a != "a" || q.Len() != len(array) {
		t.Fail()
	}
	q.PushN("Z", 2)
	if !ListEqual(q.ar[1:4], []string{"c", "Z", "d"}) || q.Len() != len(array)+1 {
		t.Fail()
	}
	d := q.PopN(3)
	if d != "d" || q.Len() != len(array) {
		t.Fail()
	}
}

func TestStringQueuePanic1(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Fail()
		}
	}()
	q := NewStringStack()
	q.PopN(999)
}

func TestStringQueuePanic2(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Fail()
		}
	}()
	q := NewStringStack()
	q.PushN("a", 999)
}
